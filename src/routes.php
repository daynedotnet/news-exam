<?php namespace App;

/* @var $app /Slim/App */

$app->get('/', function ($request, $response) {
    return $this->view->render($response, 'index.twig');
})->setName('dashboard');

$app->group('/news', function ($app) {
    $app->get('', 'App\Controllers\NewsController:index')->setName('news');
    $app->get('/create', 'App\Controllers\NewsController:create')->setName('news.create');
    $app->post('/save', 'App\Controllers\NewsController:save')->setName('news.save');
    $app->get('/view/{id:[0-9]+}', 'App\Controllers\NewsController:view')->setName('news.view');
    $app->get('/remove/{id:[0-9]+}', 'App\Controllers\NewsController:remove')->setName('news.remove');
});

$app->group('/comments', function ($app) {
    $app->get('/news_id/{id:[0-9]+}', 'App\Controllers\CommentController:index')->setName('comments');
    $app->post('/post/news_id/{id:[0-9]+}', 'App\Controllers\CommentController:post')->setName('comments.post');
    $app->get('/remove/{id:[0-9]+}/{news_id:[0-9]+}', 'App\Controllers\CommentController:remove')->setName('comments.remove');
});