<?php
// DIC configuration

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// Inject a new instance of PDO in the container
$container['db'] = function ($container) {
    $config = $container->get('settings')['pdo'];
    $dsn = "{$config['engine']}:host={$config['host']};dbname={$config['database']};charset={$config['charset']}";
    $username = $config['username'];
    $password = $config['password'];

    return new PDO($dsn, $username, $password, $config['options']);
};

// Service for twig template
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__ . '/../templates');
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new \Slim\Views\TwigExtension($container['router'], $basePath));
    $view->addExtension(new Knlv\Slim\Views\TwigMessages(
        new Slim\Flash\Messages()
    ));

    return $view;
};

// Register provider
$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

$container['App\Controllers\NewsController'] = function ($container) {
    $newsService = new \App\Services\NewsService();
    $flash = new \Slim\Flash\Messages();
    return new App\Controllers\NewsController($container, $newsService, $flash);
};

$container['App\Controllers\CommentController'] = function ($container) {
    $commentService = new \App\Services\CommentService();
    $newsService = new \App\Services\NewsService();
    $flash = new \Slim\Flash\Messages();
    return new App\Controllers\CommentController($container, $commentService, $newsService, $flash);
};