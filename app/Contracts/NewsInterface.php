<?php

namespace App\Contracts;

interface NewsInterface
{
    public function setId($id);

    public function getId();

    public function setTitle($title);

    public function getTitle();

    public function setBody($body);

    public function getBody();

    public function setCreatedAt($createdAt);

    public function getCreatedAt();
}