<?php

namespace App\Contracts;

interface CommentInterface
{
    public function setId($id);

    public function getId();

    public function setBody($body);

    public function getBody();

    public function setCreatedAt($createdAt);

    public function getCreatedAt();

    public function getNewsId();

    public function setNewsId($newsId);
}