<?php

namespace App\Controllers;

use Psr\Container\ContainerInterface;
use App\Contracts\NewsInterface;
use Slim\Flash\Messages;
use Slim\Http\Request;
use Slim\Http\Response;

class NewsController
{
    protected $container;
    protected $newsService;
    protected $flash;

    public function __construct(ContainerInterface $container, NewsInterface $newsService, Messages $flash)
    {
        $this->container = $container;
        $this->newsService = $newsService;
        $this->flash = $flash;
    }

    public function index(Request $request, Response $response)
    {
        $sql = "SELECT * FROM news ORDER BY id DESC";
        $query = $this->container['db']->prepare($sql);
        $query->execute();
        $rows = $query->fetchAll();

        $news = [];
        foreach ($rows as $row) {
            $n = new $this->newsService;
            $news[] = $n->setId($row['id'])
                ->setTitle($row['title'])
                ->setBody($row['body'])
                ->setCreatedAt($row['created_at']);
        }

        $alerts = $this->flash->getMessages();

        return $this->container['view']->render($response, 'news/index.twig', ['alerts' => $alerts, 'news' => $news]);
    }

    public function view(Request $request, Response $response, $args)
    {
        $data = [
            'id' => (int)$args['id'],
        ];
        $sql = "SELECT * FROM news WHERE id = :id";
        $query = $this->container['db']->prepare($sql);
        $query->execute($data);
        $row = $query->fetch();

        $n = new $this->newsService;
        $news = $n->setId($row['id'])
            ->setTitle($row['title'])
            ->setBody($row['body'])
            ->setCreatedAt($row['created_at']);

        if (!$news) {
            $this->flash->addMessage('danger', 'News does not exist. Please try again');
            return $response->withStatus(302)->withHeader('Location', '/news');
        }

        $alerts = $this->flash->getMessages();

        return $this->container['view']->render($response, 'news/view.twig', ['alerts' => $alerts, 'news' => $news]);
    }

    public function create(Request $request, Response $response)
    {
        return $this->container['view']->render($response, 'news/create.twig');
    }

    public function save(Request $request, Response $response, $args)
    {
        $post = $request->getParsedBody();

        if (!isset($args['id'])) {
            $data = [
                'title' => $post['title'],
                'body' => $post['body'],
                'created_at' => date('Y-m-d'),
            ];
            $sql = "INSERT INTO news (title, body, created_at) VALUES (:title, :body, :created_at)";
            $query = $this->container['db']->prepare($sql);
            if ($query->execute($data)) {
                $this->flash->addMessage('success', 'News has been created successfully');
            } else {
                $this->flash->addMessage('danger', 'An error occurred. Please try again');
            }
            return $response->withStatus(302)->withHeader('Location', '/news');
        } else {
            // No edit on the given instruction
            return false;
        }
    }

    public function remove(Request $request, Response $response, $args)
    {
        $data = [
            'id' => (int)$args['id'],
        ];
        $sql = "DELETE FROM news WHERE id = :id";
        $query = $this->container['db']->prepare($sql);
        if ($query->execute($data)) {
            $sql = "DELETE FROM comment WHERE news_id = :id";
            $query = $this->container['db']->prepare($sql);
            $query->execute($data);

            $this->flash->addMessage('success', 'News has been deleted successfully');
        } else {
            $this->flash->addMessage('danger', 'An error occurred. Please try again');
        }
        return $response->withStatus(302)->withHeader('Location', '/news');
    }

}
