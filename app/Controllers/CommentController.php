<?php

namespace App\Controllers;

use App\Contracts\NewsInterface;
use Psr\Container\ContainerInterface;
use App\Contracts\CommentInterface;
use Slim\Flash\Messages;
use Slim\Http\Request;
use Slim\Http\Response;

class CommentController
{
    protected $container;
    protected $commentService;
    protected $newsService;
    protected $flash;

    public function __construct(ContainerInterface $container, CommentInterface $commentService, NewsInterface $newsService, Messages $flash)
    {
        $this->container = $container;
        $this->commentService = $commentService;
        $this->newsService = $newsService;
        $this->flash = $flash;
    }

    public function index(Request $request, Response $response, $args)
    {
        $data = [
            'id' => (int)$args['id'],
        ];

        $sql = "SELECT * FROM comment WHERE news_id = :id ORDER BY id DESC";
        $query = $this->container['db']->prepare($sql);
        $query->execute($data);
        $rows = $query->fetchAll();

        $comments = [];
        foreach ($rows as $row) {
            $n = new $this->commentService;
            $comments[] = $n->setId($row['id'])
                ->setBody($row['body'])
                ->setCreatedAt($row['created_at'])
                ->setNewsId($row['news_id']);
        }

        $sql = "SELECT * FROM news WHERE id = :id";
        $query = $this->container['db']->prepare($sql);
        $query->execute($data);
        $row = $query->fetch();

        $n = new $this->newsService;
        $news = $n->setId($row['id'])
            ->setTitle($row['title'])
            ->setBody($row['body'])
            ->setCreatedAt($row['created_at']);

        if (!$news) {
            $this->flash->addMessage('danger', 'News does not exist. Please try again');
            return $response->withStatus(302)->withHeader('Location', '/news');
        }

        $alerts = $this->flash->getMessages();

        return $this->container['view']->render($response, 'comments/index.twig', ['alerts' => $alerts, 'news' => $news, 'comments' => $comments]);
    }

    public function post(Request $request, Response $response, $args)
    {
        $post = $request->getParsedBody();

        $data = [
            'body' => $post['body'],
            'created_at' => date('Y-m-d'),
            'news_id' => (int)$args['id'],
        ];
        $sql = "INSERT INTO comment (body, created_at, news_id) VALUES (:body, :created_at, :news_id)";
        $query = $this->container['db']->prepare($sql);
        if ($query->execute($data)) {
            $this->flash->addMessage('success', 'Comment has been posted successfully');
        } else {
            $this->flash->addMessage('danger', 'An error occurred. Please try again');
        }
        return $response->withStatus(302)->withHeader('Location', '/comments/news_id/' . $args['id'] . '');
    }

    public function remove(Request $request, Response $response, $args)
    {
        $data = [
            'id' => (int)$args['id'],
        ];
        $sql = "DELETE FROM comment WHERE id = :id";
        $query = $this->container['db']->prepare($sql);
        if ($query->execute($data)) {
            $this->flash->addMessage('success', 'Comment has been deleted successfully');
        } else {
            $this->flash->addMessage('danger', 'An error occurred. Please try again');
        }
        return $response->withStatus(302)->withHeader('Location', '/comments/news_id/'.$args['news_id'].'');
    }

}